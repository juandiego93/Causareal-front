import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { UserLogin } from '../../models/UserLogin.model'
import { UserRegister } from '../../models/UserRegister.model'
import { environment } from '../../services/../../environments/environment'
import { TokenService } from './token.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2'

@Injectable({
  providedIn: 'root'
})

export class AuthService {

  constructor(private http: HttpClient, private tokenService: TokenService, private router: Router) { }

  // User registration
  register(user: UserRegister): Observable<any> {
    return this.http.post(`${environment.serverUrl}/auth/register`, user);
  }

  // Login
  signin(user: UserLogin): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': '*',
        'Access-Control-Allow-Headers': '*',
      })
    }
    return this.http.post<any>(`${environment.serverUrl}/auth/login`, user);
  }

  logOut() {
    Swal.fire({
      title: 'Cerrando Sesión ...',
      onBeforeOpen: () => {
        Swal.showLoading()
      }
    });
    const tokenDestroy = this.tokenService.getToken();
    const logOutRequest = this.http.post(`${environment.serverUrl}/auth/logout`, tokenDestroy);

    logOutRequest
      .subscribe(
        response => {
          if (response['status']) {
            Swal.fire({
              icon: 'success',
              title: 'Has cerrado sesión correctamente',
              showConfirmButton: false,
              allowOutsideClick: false,
              timer: 1500
            })
            this.tokenService.removeToken();
            this.tokenService.isLoginSubject.next(false);
            this.router.navigate(['/'])
          }
        },
        error => {
          Swal.fire({
            icon: 'success',
            title: 'Has cerrado sesión correctamente',
            showConfirmButton: false,
            allowOutsideClick: false,
            timer: 1500
          })
          this.tokenService.removeToken();
          this.router.navigate(['/'])
        }
      );
  }

}