import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from '../../services/../../environments/environment'

@Injectable({
  providedIn: 'root'
})

export class TokenService {

  isLoginSubject = new BehaviorSubject<boolean>(this.isValidToken());

  constructor() { }

  handleData(token) {
    localStorage.setItem('auth_token', token);
  }

  getToken() {
    return localStorage.getItem('auth_token');
  }

  // Verify the token
  private isValidToken() {
    const token = this.getToken();
    console.log(token);

    if (token) {
      const payload = this.payload(token);
      console.log(payload);
      if (payload) {
        const obj = Object.values({
          login: `${environment.serverUrl}/auth/login`,
          register: `${environment.serverUrl}/auth/register`,
        }).indexOf(payload.iss) > -1 ? true : false;
        return obj
      }
    } else {
      return false;
    }
  }

  private payload(token) {
    const jwtPayload = token.split('.')[1];
    return JSON.parse(atob(jwtPayload));
  }

  // User state based on valid token
  // isLoggedIn() {
  //   const isLogg = this.isValidToken();
  //   return isLogg
  // }

  /**
*
* @returns {Observable<T>}
*/
  isLoggedIn(): Observable<boolean> {
    return this.isLoginSubject.asObservable();
  }

  // Remove token
  removeToken() {
    localStorage.removeItem('auth_token');
  }

}