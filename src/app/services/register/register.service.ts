import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UserRegister } from 'src/app/models/UserRegister.model';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor(private http: HttpClient) { }

  // User registration
  register(User: UserRegister): Observable<any> {
    return this.http.post('http://127.0.0.1:8000/api/auth/register', User);
  }
}
