import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { TokenService } from '../auth/token.service';

@Injectable({
  providedIn: 'root'
})
export class CrudCausaService {

  constructor(private tokenService: TokenService, private http: HttpClient) { }

  createCausa(idUser, causa) {
    causa.idUser = idUser;
    return this.http.post(`${environment.serverUrl}/user/create-causa`, causa);
  }

}
