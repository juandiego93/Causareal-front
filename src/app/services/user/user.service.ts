import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TokenService } from '../../services/auth/token.service'
import { environment } from '../../../environments/environment'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private tokenService: TokenService, private http: HttpClient) { }

  getUserProfile(): Observable<any> {
    const token = this.tokenService.getToken();
    const jwtPayload = token.split('.')[1];
    const idUser = JSON.parse(atob(jwtPayload))['sub'];
    return this.http.post(`${environment.serverUrl}/auth/my-profile`, idUser);
  }

}
