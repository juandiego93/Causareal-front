import { TestBed } from '@angular/core/testing';

import { CrudCausaService } from './crud-causa.service';

describe('CrudCausaService', () => {
  let service: CrudCausaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CrudCausaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
