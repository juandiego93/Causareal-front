import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/services/auth/auth.service';
import { TokenService } from 'src/app/services/auth/token.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  isLoggedIn: Observable<boolean>;

  constructor(public tokenService: TokenService, private authService: AuthService) {
    this.isLoggedIn = tokenService.isLoggedIn()
  }

  ngOnInit(): void {

  }

  logOut() {
    this.authService.logOut();
  }

}
