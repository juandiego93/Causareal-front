import { Component, OnInit } from '@angular/core';
import { Causa } from '../../models/Causa.model';
import { HomeService } from '../../services/home/home.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  causa = new Causa();
  arrayCausas = []

  constructor(private homeService: HomeService) { }

  ngOnInit(): void {
    // this.homeService.getAllCausas()
    //   .subscribe(
    //     response => {
    //       if (response.status) {
    //         this.arrayCausas = response;
    //       }
    //     },
    //     error => {
    //       return
    //       Swal.fire({
    //         icon: 'error',
    //         title: 'Ocurrio un problema al cargar las causas',
    //         showConfirmButton: true
    //       })
    //     });
  }

}
