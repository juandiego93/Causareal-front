import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UserRegister } from '../../.././../models/UserRegister.model'
import { AuthService } from 'src/app/services/auth/auth.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})

export class RegisterComponent implements OnInit {

  usuario: UserRegister;
  recordar = false
  registerForm: FormGroup;
  errors = null;

  constructor(
    private router: Router,
    public fb: FormBuilder,
    public authService: AuthService,
    private route: ActivatedRoute,
  ) {
    this.registerForm = this.fb.group({
      name: [''],
      surname: [''],
      email: [''],
      cellphone: ['']
    })

  }

  ngOnInit(): void {
    this.usuario = new UserRegister()
  }

  onSubmit(f) {
    Swal.fire({
      title: 'Espera un momento ...',
      onBeforeOpen: () => {
        Swal.showLoading()
      }
    });
    this.authService.register(f.value).subscribe(
      response => {
        if (response.status === 200) {
          Swal.fire({
            icon: 'success',
            title: 'Te has registrado correctamente. Esta es tu contraseña generada automáticamente: "' + response.password + '"',
            showConfirmButton: true,
            allowOutsideClick: false
          })
        }
        this.router.navigate(['/auth/login']);
      },
      error => {
        const message = error.error.message ? error.error.message : error.error
        Swal.fire({
          icon: 'error',
          title: 'Oops... No se realizo el registro completamente',
          text: 'Error: ' + message,
        })
        console.log(error)
        this.errors = error.error
      }
    )
  }



}
