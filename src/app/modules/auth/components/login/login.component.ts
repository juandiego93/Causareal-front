import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { TokenService } from 'src/app/services/auth/token.service';
import { AuthService } from '../../../../services/auth/auth.service';
import { UserLogin } from '../../../../models/UserLogin.model'
import Swal from 'sweetalert2'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  usuario = new UserLogin
  loginForm: FormGroup;
  errors = null;
  recordar = false

  constructor(
    public router: Router,
    public fb: FormBuilder,
    public authService: AuthService,
    private tokenService: TokenService,
  ) {
    this.loginForm = this.fb.group({
      email: [],
      password: []
    })
  }

  ngOnInit() { }

  login(f) {
    Swal.fire({
      title: 'Espera un momento ...',
      onBeforeOpen: () => {
        Swal.showLoading()
      }
    });
    this.authService.signin(f.value).subscribe(
      result => {
        if (result.access_token != '' || !('error' in result)) {
          Swal.fire({
            icon: 'success',
            title: 'Te has logueado correctamente',
            showConfirmButton: false,
            timer: 2500
          })
          this.responseHandler(result);
          this.tokenService.isLoginSubject.next(true);
          setTimeout(() => {
            this.router.navigate(['/user/my-profile']);
          }, 2000)
        }
      },
      error => {
        this.errors = error.error;
        if (error.status == 401) {
          Swal.fire({
            icon: 'error',
            title: 'Credenciales incorrectas, verifica los datos.',
            showConfirmButton: false,
            timer: 2500
          })
          this.router.navigate(['/auth/login']);
          return
        }
        Swal.fire({
          icon: 'error',
          title: 'Ocurrío un problema, prueba más tarde',
          showConfirmButton: false,
          timer: 2500
        })
        this.router.navigate(['/auth/login']);
      }
    );
  }

  // Handle response
  responseHandler(data) {
    this.tokenService.handleData(data.access_token);
  }

}