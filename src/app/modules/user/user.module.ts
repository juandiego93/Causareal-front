import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MyProfileComponent } from './components/my-profile/my-profile.component';
import { UserRoutingModule } from './user-routing.module';
import { RouterModule } from '@angular/router';
import { CrudCausaComponent } from './components/crud-causa/crud-causa.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'; 



@NgModule({

  declarations: [MyProfileComponent, CrudCausaComponent,],
  imports: [
    CommonModule,
    UserRoutingModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class UserModule { }
