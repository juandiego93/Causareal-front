import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrudCausaComponent } from './crud-causa.component';

describe('CrudCausaComponent', () => {
  let component: CrudCausaComponent;
  let fixture: ComponentFixture<CrudCausaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrudCausaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrudCausaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
