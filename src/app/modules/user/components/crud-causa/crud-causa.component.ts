import { Component, OnInit } from '@angular/core';
import { TokenService } from 'src/app/services/auth/token.service';
import { CrudCausaService } from '../../../../services/user/crud-causa.service'
import Swal from 'sweetalert2'


@Component({
  selector: 'app-crud-causa',
  templateUrl: './crud-causa.component.html',
  styleUrls: ['./crud-causa.component.css']
})
export class CrudCausaComponent implements OnInit {

  causa = {
    title: '',
    letters_key: '',
    description: '',
    firmantes: 0,
    simpatizantes: 0,
    img: ''
  }

  constructor(private crudCausa: CrudCausaService, private tokenService: TokenService) { }

  ngOnInit(): void {
  }

  createCausa(f) {
    console.log(f.value);
    const token = this.tokenService.getToken();
    const jwtPayload = token.split('.')[1];
    const idUser = JSON.parse(atob(jwtPayload))['sub'];
    Swal.fire({
      title: 'Verifica todos los campos de tu Causa',
      text: 'Al crear tu Causa no tendrás la posibilidad de modificarla así que observa y detalla que cada campo.',
      showDenyButton: true,
      // showCancelButton: true,
      allowOutsideClick: false,
      confirmButtonText: `Crear`,
      denyButtonText: `Volver`,
    }).then((result) => {
      /* Read more about isConfirmed, isDenied below */
      if (result.isConfirmed) {
        this.crudCausa.createCausa(idUser, f.value)
          .subscribe(
            response => {
              if (response['status']) {
                Swal.fire('Causa creada correctamente!', '', 'success')
              }
            },
            error => {
              console.log(error);
              Swal.fire('Ocurrio un problema al crear la Causa, intentalo más tarde', '', 'error')
            }
          );
      } else if (result.isDenied) {
        Swal.fire('Operación detenida', '', 'info')
      }
    })
  }

}
