import { Component, OnInit } from '@angular/core';
import { UserRegister } from 'src/app/models/UserRegister.model';
import { UserService } from '../../../../services/user/user.service'
@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.component.html',
  styleUrls: ['./my-profile.component.css']
})
export class MyProfileComponent implements OnInit {

  user: UserRegister;

  constructor(private userService: UserService) {
    this.user = new UserRegister();
  }

  ngOnInit(): void {
    this.userService.getUserProfile()
      .subscribe(
        response => {
          this.user = response;
        },
        error => {
          console.log(error);
        })
  }

}
