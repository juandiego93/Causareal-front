import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CrudCausaComponent } from './components/crud-causa/crud-causa.component';
import { MyProfileComponent } from './components/my-profile/my-profile.component';

const routes: Routes = [
    { path: 'my-profile', component: MyProfileComponent },
    { path: 'crud-causa', component: CrudCausaComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class UserRoutingModule { }
