import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, CanActivateChild, Router, RouterStateSnapshot } from '@angular/router';
// import { AuthStateService } from '../services/auth/auth-state.service';
import { AuthService } from '../services/auth/auth.service';
import Swal from 'sweetalert2'
import { TokenService } from '../services/auth/token.service';


@Injectable({
  providedIn: 'root'
})
export class IsAuthGuard implements CanActivate {

  constructor(private tokenService: TokenService, private authService: AuthService, private router: Router) { }

  canActivate() {
    let isAuth = this.tokenService.isLoggedIn();
    if (!isAuth) {
      this.authService.logOut();
      Swal.fire({
        icon: 'error',
        title: 'No has iniciado sesión',
        showConfirmButton: false,
        allowOutsideClick: false,
        timer: 2500
      })
      setTimeout(() => {
        this.router.navigate(['/auth/login']);
      }, 3000);
    }
    return isAuth;
  }



}
