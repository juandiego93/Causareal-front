import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './general-components/home/home.component';
import { IsAuthGuard } from './guards/is-auth.guard'

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'auth', loadChildren: () => import('./modules/auth/auth.module').then(response => response.AuthModule) },
  { path: 'user', loadChildren: () => import('./modules/user/user.module').then(response => response.UserModule), canActivate: [IsAuthGuard] },
  { path: 'admin', loadChildren: () => import('./modules/admin/admin.module').then(response => response.AdminModule), canActivate: [IsAuthGuard] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabled'
})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
