export class Causa {
    title: string;
    description: string;
    image: string;
    reasons: [];
    firms: [];
    sympathizers: []
    author: {}
}