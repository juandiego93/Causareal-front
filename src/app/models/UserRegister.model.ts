export class UserRegister {
    email: string;
    password?: string;
    name: string;
    surname: string;
    cellphone: number;
}